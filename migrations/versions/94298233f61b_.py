"""

Revision ID: 94298233f61b
Revises: 022f0450fbc0
Create Date: 2021-02-04 16:52:24.849052

"""

# revision identifiers, used by Alembic.
revision = '94298233f61b'
down_revision = '022f0450fbc0'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('firmware', sa.Column('signed_epoch', sa.Integer(), nullable=True))


def downgrade():
    op.drop_column('firmware', 'signed_epoch')
