#!/bin/bash
set -e

function ecs_metadata {
    if [ -n "$ECS_CONTAINER_METADATA_URI_V4" ]; then
        curl ${ECS_CONTAINER_METADATA_URI_V4} --output /tmp/container.json
        export CONTAINER_ID=`jq -r -s '.[0].DockerId' /tmp/container.json`
    else
        export CONTAINER_ID="unset"
    fi
    echo "CONTAINER_ID is $CONTAINER_ID"
}

if [ "$DEPLOY" = "application" ]
then
    ecs_metadata
    echo "upgrading database"
    FLASK_APP=lvfs/__init__.py flask db upgrade
    echo "starting gunicorn"
    exec gunicorn --config /app/conf/gunicorn.py "lvfs:app"
fi

if [ "$DEPLOY" = "metadata" ]
then
    ecs_metadata
    echo "starting freshclam"
    freshclam --no-dns # download our files first
    freshclam -v --stdout -d -c 1 --daemon-notify=/etc/clamd.d/scan.conf # then daemonize
    echo "starting clam"
    /usr/sbin/clamd -c /etc/clamd.d/scan.conf
    echo "upgrading database"
    FLASK_APP=lvfs/__init__.py flask db upgrade
    echo "starting celery worker with queues [metadata,firmware,celery,yara]"
    exec celery -A lvfs.tq worker --uid nobody --gid nobody --queues metadata,firmware,celery,yara --loglevel DEBUG
fi

if [ "$DEPLOY" = "beat" ]
then
    ecs_metadata
    echo "upgrading database"
    FLASK_APP=lvfs/__init__.py flask db upgrade
    echo "starting celery beat"
    exec celery -A lvfs.tq beat --uid nobody --gid nobody --loglevel DEBUG
fi

if [ "$DEPLOY" = "test" ]
then
    dnf install -y redis
    source /app/venv/bin/activate
    pip install mypy
    redis-server &
fi
