FROM centos:8
EXPOSE 5000

RUN dnf install -y epel-release dnf-plugins-core \
	&& dnf config-manager --set-enabled powertools epel \
	&& dnf -y copr enable rhughes/lvfs-website \
	&& dnf -y --setopt=install_weak_deps=False --best install \
	bsdtar \
	clamav \
	clamav-update \
	clamd \
	radare2 \
	jq \
	python3-pip \
	UEFITool \
	&& rm -rf /var/cache/dnf

# create all our dirs
RUN bash -c 'mkdir -p /app/{scripts,conf,logs/uwsgi}' \
	&& mkdir /data /backups
WORKDIR /app

# create and activate a venv
ENV VIRTUAL_ENV=/app/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# install all the things
COPY requirements.txt /app/conf
COPY contrib/*/*.whl /tmp/
RUN pip3 install --no-cache-dir --upgrade pip \
	&& pip3 install --no-cache-dir wheel \
	&& pip3 install --no-cache-dir /tmp/*.whl \
	&& pip3 install --no-cache-dir -r conf/requirements.txt

# copy the app; various configs and scripts
COPY lvfs/ /app/lvfs/
COPY pkgversion/ /app/pkgversion/
COPY jcat/ /app/jcat/
COPY infparser/ /app/infparser/
COPY plugins/ /app/plugins/
COPY migrations/ /app/migrations/
COPY docker/files/application/gunicorn.py /app/conf/gunicorn.py
COPY docker/files/application/flaskapp.cfg /app/lvfs/flaskapp.cfg
COPY docker/files/application/scan.conf /etc/clamd.d/scan.conf
COPY docker/files/lvfs-entrypoint.sh /app/lvfs-entrypoint.sh

RUN chown -R 65534:65534 /app /data /backups

ENTRYPOINT [ "./lvfs-entrypoint.sh" ]
