#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=no-self-use,line-too-long

import os
import fnmatch
import json
from typing import Dict
from urllib.parse import urljoin

import requests

from lvfs import db

from lvfs.firmware.models import Firmware
from lvfs.pluginloader import PluginBase, PluginError
from lvfs.pluginloader import PluginSetting, PluginSettingBool


class Plugin(PluginBase):
    def __init__(self):
        PluginBase.__init__(self, "ipfs")
        self.name = "IPFS"
        self.summary = "Sync public firmware to IPFS"
        self.settings.append(PluginSettingBool(key="ipfs_enable", name="Enabled"))
        self.settings.append(
            PluginSetting(
                key="ipfs_uri",
                name="URI",
                default="https://api.pinata.cloud/pinning/",
            )
        )
        self.settings.append(PluginSetting(key="ipfs_api_key", name="API Key"))
        self.settings.append(PluginSetting(key="ipfs_secret_key", name="Secret Key"))

    @property
    def _headers(self) -> Dict[str, str]:
        headers: Dict[str, str] = {}
        api_key = self.get_setting("ipfs_api_key")
        if api_key:
            headers["pinata_api_key"] = api_key
        secret_key = self.get_setting("ipfs_secret_key")
        if api_key:
            headers["pinata_secret_api_key"] = secret_key
        return headers

    def _ipfs_unpin(self, fw: Firmware) -> None:
        url = urljoin(self.get_setting("ipfs_uri", required=True), "unpin", fw.ipfs)
        print("Unpinning {} from IPFS".format(fw.filename))
        headers = self._headers
        headers["Content-Type"] = "application/json"
        r = requests.delete(url, headers=headers)
        if r.status_code != 200:
            raise PluginError("Failed to unpin: " + r.text)
        fw.ipfs = None
        db.session.commit()

    def _ipfs_pin(self, fw: Firmware) -> None:
        url = urljoin(self.get_setting("ipfs_uri", required=True), "pinFileToIPFS")
        try:
            files = {
                "file": (
                    fw.filename,
                    open(fw.absolute_path, "rb"),
                    "application/vnd.ms-cab-compressed",
                    {"name": fw.filename, "keyvalues": {"FirmwareId": fw.firmware_id}},
                )
            }
        except FileNotFoundError as e:
            raise PluginError("Failed to load file") from e
        print("Pinning {} to IPFS".format(fw.filename))
        r = requests.post(url, headers=self._headers, files=files)
        if not r.text:
            raise PluginError("No request content")
        try:
            response = json.loads(r.text)
            fw.ipfs = response["IpfsHash"]
        except (KeyError, ValueError) as e:
            raise PluginError("Failed to pin: " + r.text) from e
        db.session.commit()

    def archive_hash(self, fw: Firmware) -> None:

        # delete
        if fw.ipfs:
            self._ipfs_unpin(fw)

        # pin if enabled
        if fw.enable_ipfs:
            self._ipfs_pin(fw)
