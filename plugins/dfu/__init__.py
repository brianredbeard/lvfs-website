#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=no-self-use

import struct
import zlib

from lvfs.pluginloader import PluginBase, PluginError, PluginSettingBool
from lvfs.tests.models import Test


class Plugin(PluginBase):
    def __init__(self):
        PluginBase.__init__(self)
        self.name = "DFU"
        self.summary = "Check the DFU firmware footer"
        self.settings.append(
            PluginSettingBool(key="dfu_check_footer", name="Enabled", default=True)
        )

    def require_test_for_md(self, md):

        if not md.blob:
            return False
        if not md.protocol:
            return False
        return md.protocol.value == "org.usb.dfu"

    def ensure_test_for_fw(self, fw):

        # add if not already exists
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id, waivable=True)
            fw.tests.append(test)

    def run_test_on_md(self, test, md):

        # unpack the footer
        sz = len(md.blob)
        if sz < 16:
            test.add_fail("FileSize", "0x{:x}".format(sz))
            # we have to abort here, no further tests are possible
            return

        footer = md.blob[sz - 16 :]
        try:
            (
                bcdDevice,
                idProduct,
                idVendor,
                bcdDFU,
                ucDfuSig,
                bLength,
                dwCRC,
            ) = struct.unpack("<HHHH3sBL", footer)
        except struct.error as _:
            test.add_fail("Footer", footer)
            # we have to abort here, no further tests are possible
            return

        # check ucDfuSig
        if ucDfuSig != b"UFD":
            test.add_fail("Footer Signature", "{} is not valid".format(ucDfuSig))
            # we have to abort here, no further tests are possible
            return

        # check bcdDevice
        if bcdDevice in [0x0000, 0xFFFF]:
            test.add_fail("Device", "0x{:04x} is not valid".format(bcdDevice))

        # check idProduct
        if idProduct in [0x0000, 0xFFFF]:
            test.add_fail("Product", "0x{:04x} is not valid".format(idProduct))

        # check idVendor
        if idVendor == 0x0000:
            test.add_fail("Vendor", "0x{:04x} is not valid".format(idVendor))

        # check bcdDFU
        if bcdDFU in [0x0101, 0x0100]:
            test.add_pass("DFU Version", "0x{:04x}".format(bcdDFU))
        else:
            test.add_fail("DFU Version", "0x{:04x} is not valid".format(bcdDFU))

        # check bLength
        if bLength >= 10:
            test.add_pass("DFU Length", "0x{:02x}".format(bLength))
        else:
            test.add_fail("DFU Length", "0x{:02x} is not valid".format(bLength))

        # check dwCRC
        crc_correct = zlib.crc32(md.blob[: sz - 4]) ^ 0xFFFFFFFF
        if dwCRC == crc_correct:
            test.add_pass("CRC", "0x{:04x}".format(dwCRC))
        else:
            test.add_fail(
                "CRC",
                "0x{:04x} invalid, expected 0x{:04x}".format(dwCRC, crc_correct),
            )


# run with PYTHONPATH=. ./env/bin/python3 plugins/dfu/__init__.py
if __name__ == "__main__":
    import sys

    from lvfs.firmware.models import Firmware
    from lvfs.protocols.models import Protocol
    from lvfs.components.models import Component

    for _argv in sys.argv[1:]:
        print("Processing", _argv)
        plugin = Plugin()
        _test = Test(plugin_id=plugin.id)
        _fw = Firmware()
        _md = Component()
        _md.component_id = 999999
        _md.filename_contents = "filename.bin"
        _md.protocol = Protocol(value="org.usb.dfu")
        with open(_argv, "rb") as _f:
            _md.blob = _f.read()
        plugin.run_test_on_md(_test, _md)
        for attribute in _test.attributes:
            print(attribute)
