#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=no-self-use,protected-access

import json
from typing import Optional, Dict

import datetime
import dateutil.parser
import requests

from lvfs.firmware.models import Firmware
from lvfs.components.models import Component, ComponentIssue
from lvfs.pluginloader import PluginBase, PluginError, PluginSetting, PluginSettingBool
from lvfs import ploader, db


class Plugin(PluginBase):
    def __init__(self):
        PluginBase.__init__(self, "vince")
        self.name = "VINCE"
        self.summary = "Add VU# security IDs and auto-set the embargo date"
        self.settings.append(PluginSettingBool(key="vince_enable", name="Enabled"))
        self.settings.append(
            PluginSetting(
                key="vince_apikey",
                name="API Key",
                default="deadbeef",
            )
        )

    def _find_dt_for_vu_issue(
        self, issue: ComponentIssue
    ) -> Optional[datetime.datetime]:

        apikey = self.get_setting("vince_apikey", required=True)
        api = "https://kb.cert.org/vince/comm/api/case/{}/".format(issue.value)
        headers = {"Authorization": "Token {}".format(apikey)}
        r = requests.get(api, headers=headers, stream=True)

        # probably permissions error
        if r.status_code != 200:
            return None

        # parse JSON
        try:
            data = json.loads(r.text)
        except json.decoder.JSONDecodeError as e:
            raise PluginError("Failed to query: {}".format(r.text)) from e

        # parse date
        try:
            if not data["due_date"]:
                return None
            restricted_ts = dateutil.parser.parse(data["due_date"]).replace(tzinfo=None)
        except KeyError as e:
            raise PluginError("Failed to get due_date: {}".format(r.text)) from e
        except dateutil.parser._parser.ParserError as e:  # type: ignore
            raise PluginError("Failed to parse due_date: {}".format(r.text)) from e
        return restricted_ts

    def _autoadd_vince_for_cve_text(self, md: Component, r_text: str) -> None:

        # parse JSON
        try:
            data = json.loads(r_text)
        except json.decoder.JSONDecodeError as e:
            raise PluginError("Failed to query: {}".format(r_text)) from e

        # parse VUID
        try:
            vuid = data["note"]["vuid"]
            if vuid.startswith("VU#"):
                vuid = vuid[3:]
        except KeyError as e:
            raise PluginError("Failed to get VUID: {}".format(r_text)) from e

        # does this component have the VU# issue already?
        if vuid in md.issue_values:
            return
        issue = ComponentIssue(kind="vince", value=vuid, user_id=2)
        md.issues.append(issue)
        print(
            "added {} to component {} using VINCE".format(
                issue.value_display, md.component_id
            )
        )
        db.session.commit()

    def _autoadd_vince_for_cve(self, issue: ComponentIssue) -> None:

        # public search
        api = "https://kb.cert.org/vuls/api/vuls/cve/{}".format(issue.value[4:])
        r = requests.get(api, stream=True)
        if r.status_code == 200:
            self._autoadd_vince_for_cve_text(issue.md, r.text)

        # private search
        apikey = self.get_setting("vince_apikey", required=True)
        api = "https://kb.cert.org/vince/comm/api/cve/{}/".format(issue.value[4:])
        headers = {"Authorization": "Token {}".format(apikey)}
        r = requests.get(api, headers=headers, stream=True)
        if r.status_code == 200:
            self._autoadd_vince_for_cve_text(issue.md, r.text)

    def archive_presign(self, fw: Firmware) -> None:

        # add VU# IDs for CVEs
        for md in fw.mds:
            for issue in md.issues:
                if issue.kind in ["cve", "intel-sa"]:
                    self._autoadd_vince_for_cve(issue)

        # update the embargo date
        dt_largest = fw.restricted_ts
        for md in fw.mds:
            for issue in md.issues:
                if issue.kind == "vince":
                    dt = self._find_dt_for_vu_issue(issue)
                    if not dt:
                        continue
                    if not dt_largest or dt > dt_largest:
                        dt_largest = dt

        # changed
        if fw.restricted_ts != dt_largest:
            print(
                "updated firmware {} restricted_ts from {} to {} using VINCE".format(
                    fw.firmware_id, fw.restricted_ts, dt_largest
                )
            )
            fw.restricted_ts = dt_largest
            db.session.commit()


# run with PYTHONPATH=. ./env/bin/python3 plugins/vince/__init__.py
if __name__ == "__main__":
    plugin = Plugin()
    _fw = Firmware()
    _md = Component()
    _md.issues.append(ComponentIssue(kind="vince", value="999999"))
    _md.issues.append(ComponentIssue(kind="cve", value="CVE-2020-11902"))
    _md.issues.append(ComponentIssue(kind="vince", value="257161"))
    _fw.mds.append(_md)
    plugin.archive_presign(_fw)
    print("restricted_ts", _fw.restricted_ts)
    print("issues", _md.issues)
