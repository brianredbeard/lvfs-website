# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
# SPDX-License-Identifier: GPL-2.0+

VENV=./env
PYTHON=$(VENV)/bin/python
PYTEST=$(VENV)/bin/pytest
PYLINT=$(VENV)/bin/pylint
MYPY=$(VENV)/bin/mypy
SPHINX_BUILD=$(VENV)/bin/sphinx-build
FLASK=$(VENV)/bin/flask
CODESPELL=$(VENV)/bin/codespell
PIP=$(VENV)/bin/pip
PYUP=$(VENV)/bin/pyup
BLACK=$(VENV)/bin/black

setup: requirements.txt
	virtualenv ./env
	$(VENV)/bin/pip install -r requirements.txt

clean:
	rm -rf ./build
	rm -rf ./htmlcov

run:
	FLASK_DEBUG=1 FLASK_APP=lvfs/__init__.py $(VENV)/bin/flask run

profile:
	FLASK_DEBUG=1 FLASK_APP=lvfs/__init__.py $(VENV)/bin/python run-profile.py

dbup:
	FLASK_APP=lvfs/__init__.py $(FLASK) db upgrade

dbdown:
	FLASK_APP=lvfs/__init__.py $(FLASK) db downgrade

freeze:
	$(PIP) freeze > requirements.txt

dbmigrate:
	FLASK_APP=lvfs/__init__.py $(FLASK) db migrate

docs:
	$(SPHINX_BUILD) docs build

wheels:
	python3.6 -m venv env36
	./env36/bin/pip install wheel
	./env36/bin/pip install yara-python

codespell:
	$(CODESPELL) --write-changes --builtin en-GB_to_en-US --skip \
	.git,\
	.mypy_cache,\
	.coverage,\
	*.pyc,\
	*.cab,\
	*.png,\
	*.jpg,\
	*.js,\
	*.doctree,\
	*.pdf,\
	*.gz,\
	*.ico,\
	*.jcat,\
	*.pickle,\
	*.key,\
	env,\
	shards,\
	owl.carousel.js,\
	celerybeat-schedule

$(PYTEST):
	$(PIP) install pytest-cov pylint pytest-pylint pyserial

$(MYPY):
	$(PIP) install mypy

$(PYUP):
	$(PIP) install pyupio

$(BLACK):
	$(PIP) install black

blacken:
	find \
	cabarchive \
	contrib \
	infparser \
	jcat \
	lvfs \
	pkgversion \
	plugins \
	-name '*.py' -exec $(BLACK) {} \;

pyup: $(PYUP)
	$(PYUP) --provider gitlab --repo=fwupd/lvfs-website --user-token=`cat ~/.config/pyup.txt`

check: $(PYTEST) $(MYPY) contrib/blocklist.cab contrib/chipsec.cab
	$(PYTEST) \
		--cov=lvfs \
		--cov=pkgversion \
		--cov=infparser \
		--cov=plugins \
		--cov-report=html
	$(MYPY) jcat pkgversion lvfs plugins contrib migrations/versions
