#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

import hashlib
import hmac
import datetime

from lvfs import app, db, tq
from lvfs.vendors.models import Vendor, VendorAffiliation
from lvfs.firmware.models import Firmware
from lvfs.metadata.models import Remote

from lvfs.dbutils import _execute_count_star


def _vendor_hash(vendor: Vendor) -> str:
    """ Generate a HMAC of the vendor name """
    return hmac.new(
        key=app.config["SECRET_VENDOR_SALT"].encode(),
        msg=vendor.group_id.encode(),
        digestmod=hashlib.sha256,
    ).hexdigest()


def _update_attrs_vendor(v: Vendor) -> None:

    # is an ODM if affiliation is set up
    v.is_odm = (
        db.session.query(VendorAffiliation.affiliation_id)
        .filter(VendorAffiliation.vendor_id_odm == v.vendor_id)
        .first()
        is not None
    )

    # 26 weeks is half a year or about 6 months
    now = datetime.datetime.utcnow() - datetime.timedelta(weeks=26)
    v.fws_stable_recent = _execute_count_star(
        db.session.query(Firmware.firmware_id)
        .join(Firmware.remote)
        .filter(
            Remote.name == "stable",
            Firmware.vendor_id == v.vendor_id,
            Firmware.timestamp > now,
        )
    )
    v.fws_stable = _execute_count_star(
        db.session.query(Firmware.firmware_id)
        .join(Firmware.remote)
        .filter(Firmware.vendor_id == v.vendor_id, Remote.name == "stable")
    )


@tq.task(max_retries=3, default_retry_delay=60, task_time_limit=10)
def _async_update_attrs_vendor(vendor_id: int) -> None:
    v = db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).first()
    if v:
        _update_attrs_vendor(v)
        db.session.commit()


@tq.task(max_retries=3, default_retry_delay=360, task_time_limit=60)
def _async_update_attrs_vendors() -> None:
    for v in db.session.query(Vendor):
        _update_attrs_vendor(v)
    db.session.commit()
