#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison

import json
import time
import datetime
from collections import defaultdict
from typing import Dict, Any

from flask import (
    Blueprint,
    Response,
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_login import login_required

from lvfs import db, auth

from lvfs.util import admin_login_required, _error_internal, _get_settings
from lvfs.components.models import Component, ComponentIssue
from lvfs.firmware.models import Firmware
from lvfs.firmware.utils import _async_sign_fw, _async_upload_ipfs
from lvfs.licenses.models import License
from lvfs.users.models import User
from lvfs.vendors.models import Vendor
from lvfs.vendors.utils import _async_update_attrs_vendors
from lvfs.metadata.models import Remote
from lvfs.analytics.utils import _async_generate_stats_all

from .utils import _async_fsck_update_descriptions, _async_fsck_eventlog_delete

bp_fsck = Blueprint("fsck", __name__, template_folder="templates")


@bp_fsck.route("/")
@login_required
@admin_login_required
def route_view():
    return render_template("fsck.html", category="admin")


@bp_fsck.route("/update_descriptions", methods=["POST"])
@login_required
@admin_login_required
def route_update_descriptions():

    for key in ["search", "replace"]:
        if key not in request.form or not request.form[key]:
            return _error_internal("No %s specified!" % key)

    # asynchronously rebuilt
    flash("Updating update descriptions", "info")
    _async_fsck_update_descriptions.apply_async(
        args=(
            request.form["search"],
            request.form["replace"],
        ),
        queue="metadata",
    )

    return redirect(url_for("fsck.route_view"))


@bp_fsck.route("/eventlog/delete", methods=["POST"])
@login_required
@admin_login_required
def route_eventlog_delete():

    for key in ["value"]:
        if key not in request.form or not request.form[key]:
            return _error_internal("No %s specified!" % key)

    # asynchronously rebuilt
    flash("Deleting eventlog entries", "info")
    _async_fsck_eventlog_delete.apply_async(
        args=(request.form["value"],),
    )

    return redirect(url_for("fsck.route_view"))


@bp_fsck.route("/generate_stats", methods=["POST"])
@login_required
@admin_login_required
def route_generate_stats():

    # asynchronously rebuilt
    flash("Generating stats", "info")
    _async_generate_stats_all.apply_async()
    return redirect(url_for("fsck.route_view"))


@bp_fsck.route("/route_update_attrs_vendors", methods=["POST"])
@login_required
@admin_login_required
def route_update_attrs_vendors():

    # asynchronously rebuilt
    flash("Updating vendor attributes", "info")
    _async_update_attrs_vendors.apply_async()
    return redirect(url_for("fsck.route_view"))


@bp_fsck.route("/upload_ipfs", methods=["POST"])
@login_required
@admin_login_required
def route_upload_ipfs():

    # asynchronously rebuild
    flash("Uploading IPFS content", "info")
    _async_upload_ipfs.apply_async()
    return redirect(url_for("fsck.route_view"))


@bp_fsck.route("/healthcheck")
@auth.login_required
def route_healthcheck():
    """ this is unauthenticated!! """

    # worst case scenario
    item: Dict[str, Any] = {}
    item["DatabaseRead"] = False
    item["DatabaseWrite"] = False
    item["FirmwareSigning"] = False
    item["MetadataSigning"] = False

    try:
        # find the first private firmware uploaded by the admin
        stmt = (
            db.session.query(Vendor.vendor_id)
            .filter(Vendor.group_id == "admin")
            .subquery()
        )
        fw = (
            db.session.query(Firmware)
            .join(stmt, Firmware.vendor_id == stmt.c.vendor_id)
            .join(Remote)
            .filter(Remote.name == "private")
            .order_by(Firmware.firmware_id.asc())
            .limit(1)
            .one()
        )
        item["DatabaseRead"] = True

        # mark firmware unsigned
        fw.signed_timestamp = None
        fw.mark_dirty()
        db.session.commit()
        item["DatabaseWrite"] = True

        # sign file
        _async_sign_fw.apply_async(args=(fw.firmware_id,), queue="firmware")
        for _ in range(9):
            time.sleep(1)
            db.session.expire_all()
            if fw.signed_timestamp:
                break
        if fw.signed_timestamp:
            item["FirmwareSigning"] = True

        # check metadata was signed
        if fw.remote.is_dirty:
            item["MetadataSigning"] = True

    except Exception as e:  # pylint: disable=broad-except
        item["Exception"] = str(e)

    dat = json.dumps(item, indent=4, separators=(",", ": "))
    return Response(response=dat, status=200, mimetype="application/json")


@bp_fsck.route("/lockdown", methods=["POST"])
@login_required
@admin_login_required
def route_lockdown():

    # this is unrecoverable
    for user in db.session.query(User).filter(User.user_id != 1):
        user.auth_type = "disabled"
    db.session.commit()
    flash("Disabled all users", "warning")

    return redirect(url_for("fsck.route_view"))


@bp_fsck.route("/issues", methods=["POST"])
@login_required
@admin_login_required
def route_issues():

    cnt = 0
    for component_issue_id in db.session.query(ComponentIssue.component_issue_id):
        issue = (
            db.session.query(ComponentIssue)
            .filter(ComponentIssue.component_issue_id == component_issue_id)
            .one()
        )
        if issue.value != issue.value.upper():
            issue.value = issue.value.upper()
            cnt += 1
        if not issue.user:
            issue.user = issue.md.fw.user
            cnt += 1
        if not issue.timestamp:
            issue.timestamp = issue.md.fw.timestamp
            cnt += 1
    db.session.commit()
    flash("Fixed up {} issues".format(cnt), "info")
    return redirect(url_for("fsck.route_view"))


@bp_fsck.route("/project_license", methods=["POST"])
@login_required
@admin_login_required
def route_project_license():

    # create map of all licenses
    license_map: Dict[str, License] = {}
    for lic in db.session.query(License):
        license_map[lic.value] = lic
    lic = license_map.get("LicenseRef-proprietary")
    if lic:
        license_map["proprietary"] = lic
        license_map["proprietary0"] = lic
        license_map["Proprietary"] = lic
    lic = license_map.get("GPL-3.0")
    if lic:
        license_map["GPLv3"] = lic
    lic = license_map.get("GPL-2.0")
    if lic:
        license_map["GPLv2"] = lic
    lic = license_map.get("BSD-2-Clause-Patent")
    if lic:
        license_map["BSD-2-Clause Plus Patent"] = lic
    lic = license_map.get("CC0-1.0")
    if lic:
        license_map["CC0 1.0"] = lic
    lic = license_map.get("MIT")
    if lic:
        license_map["Apache 2.0 OR MIT"] = lic

    cnt = 0
    unknown_licenses: Dict[str, int] = defaultdict(int)

    # fixup project license
    for component_id in (
        db.session.query(Component.component_id)
        .filter(Component.project_license_id == None)
        .limit(200)
    ):
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .one()
        )
        if not md.project_license and md.unused_project_license:
            md.project_license = license_map.get(md.unused_project_license)
            if md.project_license:
                cnt += 1
            else:
                unknown_licenses[md.unused_project_license] += 1

    # fixup metadata license
    for component_id in (
        db.session.query(Component.component_id)
        .filter(Component.metadata_license_id == None)
        .limit(200)
    ):
        md = (
            db.session.query(Component)
            .filter(Component.component_id == component_id)
            .one()
        )
        if not md.metadata_license and md.unused_metadata_license:
            md.metadata_license = license_map.get(md.unused_metadata_license)
            # some early Lenovo ThinkPad firmware (before we were validating the
            # correct values on upload); cleared with Lenovo Legal.
            if (
                md.metadata_license
                and md.metadata_license.value == "LicenseRef-proprietary"
            ):
                md.metadata_license = license_map.get("CC0-1.0")
            if md.metadata_license:
                cnt += 1
            else:
                unknown_licenses[md.unused_metadata_license] += 1

    db.session.commit()

    # success
    if cnt:
        flash("Fixed {} license values".format(cnt), "info")
    else:
        flash("No license values to fix", "info")
    if unknown_licenses:
        tmp = ",".join(unknown_licenses.keys())
        flash("Unknown license values {}".format(tmp), "warning")
    else:
        flash("No unknown license values", "info")
    return redirect(url_for("fsck.route_view"))


@bp_fsck.route("/resign", methods=["POST"])
@login_required
@admin_login_required
def route_resign():

    # unset the signed timestamp as required
    settings = _get_settings()
    signed_epoch = int(settings["signed_epoch"])
    for (firmware_id,) in (
        db.session.query(Firmware.firmware_id)
        .filter(Firmware.signed_timestamp != None)
        .order_by(Firmware.firmware_id.asc())
    ):
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
        # we signed this recently enough to be epoch 1
        if fw.signed_timestamp.replace(tzinfo=None) > datetime.datetime(2020, 3, 5):
            fw.signed_epoch = 1
        # not good enough
        if fw.signed_epoch != signed_epoch:
            fw.signed_timestamp = None
    db.session.commit()

    # unsigned firmware, or firmware signed with an older key (or method)
    firmware_ids = (
        db.session.query(Firmware.firmware_id)
        .filter(Firmware.signed_timestamp == None)
        .order_by(Firmware.firmware_id.asc())
        .all()
    )
    if firmware_ids:
        for (firmware_id,) in firmware_ids:
            _async_sign_fw.apply_async(args=(firmware_id,), queue="firmware")
        flash("Resigning {} firmwares".format(len(firmware_ids)), "info")
    else:
        flash("No firmware requires signing", "info")
    return redirect(url_for("fsck.route_view"))
