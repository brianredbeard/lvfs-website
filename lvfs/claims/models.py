#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods

from typing import Any, Optional

from sqlalchemy import Column, Integer, Text

from lvfs import db


class Claim(db.Model):

    __tablename__ = "claims"

    claim_id = Column(Integer, primary_key=True)
    kind = Column(Text, nullable=False, index=True)
    icon = Column(Text)  # e.g. 'success'
    summary = Column(Text)
    description = Column(Text, default=None)
    url = Column(Text)

    # private
    allow_embargo: bool = True

    def __lt__(self, other: Any) -> bool:
        if self.icon != other.icon:
            return self.icon < other.icon
        return self.kind < other.kind

    def __eq__(self, other: Any) -> bool:
        return self.kind == other.kind

    @property
    def icon_name(self) -> Optional[str]:
        if self.icon in ["danger", "warning"]:
            return "times-circle"
        if self.icon == "info":
            return "info-circle"
        if self.icon == "success":
            return "check-circle"
        if self.icon == "waiting":
            return "clock"
        return self.icon

    def __repr__(self) -> str:
        return "Claim object {}:{}->{}".format(self.claim_id, self.kind, self.icon)
